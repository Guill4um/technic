Technic
=======


Digiline on Machines
-------

Some **machines** support some digiline event and digiline command:
* electric furnace
* compressor
* compactor
* extractor
* alloying furnace
* centrifuge
* sawmill
* thresher
  
#### Machines Digiline Event:
when you add a channel it activates digiline event on those machines:
  ###### Work Event
this event is thrown when :
- the machine is **Starting** to process some material after idle time. You will receive a table like this : `{event="work", status = "Starting"}`
- the machine is **Ending** to process some material and is changing to idle state. You will receive a table like this : `{event="work", status = "Ending"}`
- the machine **Stalled** (output inventory is full) : `{type="work", status = "Stalled"}`
- the machine is **Restarting** after being stalled : `{type="work", status = "Restarting"}`


#### Machines Digiline command:
   ###### "state" command
   synthax: <state> 
   example: `digiline_send("myChannel","state")`
   sending the "state" digiline command, you will recieve informations about you machine state, it's a message with those informations :    
   ```lua
{ 
  type ="state",
  msg = { 
    state="Active"|"Idle"|"Unpowered"|"Stalled", 
    source={
      {name ="my:nodename",max=99,count=75},
      ... -- other stack (for example in alloying furnace)
    }
  }
}
   ```

**Example:**
```lua 
-- Example 1: sending commande with digiline :
 digiline_send("myChannel","state")
-- you will recieve a table like this:
--{ 
--  type ="state",
--  msg = { 
--    state="Active",
--    source={
--      {max=99, count= 75, name="default:cobble"}, -- stack one
--      {max=99, count=99, name="default:wood"} -- you can got other --stacks , for example in alloying furnace.
--    },
--  }
--}


-- Example 2 use digiline response in a lua tube/controller:
if event.type == "state" then
  -- in a furnace only one slot in source inventory.
  local progress = event.msg.source[1].count
  local max = event.msg.source[1].max
  local percent = 100.0 * progress / max 
end
```
   ###### "eject_input" command
   synthax: eject_input [top|bottom|right|left|front|back]
   example: `digiline_send("myChannel","eject_input back")`
   the eject command allow you to eject remaining material in the source list (material that are not processed yet). 

  -  _top is default direction: sending only "eject_input" is like sending "eject_input top"_
  -  if a tube is connected on the choosen direction, the stack is injected , else the stack is drop.



Credits
-------
Credits for contributing to the project (in alphabetical order):
  * kpoppel
  * Nekogloop
  * Nore/Ekdohibs
  * ShadowNinja
  * VanessaE
  * And many others...

FAQ
---

1. My technic circuit doesn't work.  No power is distrubuted.
  * A: Make sure you have a switching station connected.

License
-------

Unless otherwise stated, all components of this modpack are licensed under the 
LGPL, V2 or later.  See also the individual mod folders for their
secondary/alternate licenses, if any.
